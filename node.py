#!/usr/bin/env python3
"""
Module: node.py
General trees.
"""

import sys
import random
import math
import time

__author__ = 'Jaanus'


class Node(object):
    """
    Class Node.
    Tree node with two pointers.
    """
    __name = 'noname_node'
    __first_child = None
    __next_sibling = None
    __info = 0

    def __init__(self, name, down, right):
        self.__name = name
        self.__first_child = down
        self.__next_sibling = right

    def get_name(self):
        return self.__name

    def set_name(self, name):
        self.__name = name

    def get_first_child(self):
        return self.__first_child

    def set_first_child(self, node):
        self.__first_child = node

    def get_next_sibling(self):
        return self.__next_sibling

    def set_next_sibling(self, node):
        self.__next_sibling = node

    def get_info(self):
        return self.__info

    def set_info(self, info):
        self.__info = info

    def size(self):
        res = 1
        child = self.get_first_child()
        while child:
            res += child.size()
            child = child.get_next_sibling()
        return res

    def process_node(self):
        print(self.get_name(), end=' ')

    def pre_order(self):
        self.process_node()
        child = self.get_first_child()
        while child:
            child.pre_order()
            child = child.get_next_sibling()

    @staticmethod
    def create_tree():
        return Node('A',
                    Node('B',
                         None,
                    Node('C',
                         Node('D',
                              None,
                         None),
                    Node('E',
                         None,
                    None))),
               None)


def main():
    """
    Main method.
    """
    tree = Node.create_tree()
    tree.pre_order()
    print()
    suurus = tree.size()
    print("Suurus: ", suurus)


if __name__ == '__main__':
    main()
